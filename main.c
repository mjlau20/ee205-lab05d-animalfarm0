///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
///
/// @file    animalFarm0.c
/// @version 1.0
///
/// @author Michael Lau <mjlau20hawaii.edu>
/// @date   22_Feb_2022 
///////////////////////////////////////////////////////////////////////////////
//

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   printf( "Starting Animal Farm 0\n" );
   printf( "Done with Animal Farm 0\n" );

}
