###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### @author Michael Lau <mjlau20hawaii.edu>
### @date   22_Feb_2022
###############################################################################

CC 	 = gcc
CFLAGS = -g -Wall -Wextra

TARGET = main

all: $(TARGET)

catDatabase.o: catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.h 

addCats.o: addCats.c catDatabase.o
	$(CC) $(CFLAGS) -c addCats.c catDatabase.h 

clean:
	rm -f $(TARGET) *.o
