///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
///
/// @file    catDatabase.h
/// @version 1.0
///
/// @author Michael Lau <mjlau20hawaii.edu>
/// @date   22_Feb_2022 
///////////////////////////////////////////////////////////////////////////////
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_CATS     (30)
#define CURRENT_CATS (size_t)

char  name[MAX_CATS];
float weight[MAX_CATS];
bool  isFixed[MAX_CATS]; 
enum  Gender { UNKNOWN_GENDER, FEMALE, MALE } gender; 
enum  Breed  { UNKNOWN_BREED, MAINE_COON, SHORTHAIR, PERSIAN, SPHYNX, MANX } breed;
